import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class green here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class green extends Actor
{
    /**
     * Act - do whatever the green wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       moveUp();
       pushKeys();
    } 
    
    private void moveUp()
    {
        setLocation(getX(), getY()-4);
    }   
    
        private void pushKeys()
    {
        if (this.isAtEdge())
        {
           MyWorld world = (MyWorld)getWorld();
           world.addScore(-1);
           world.removeObject(this);
        }
        else if (Greenfoot.isKeyDown("s") && this!=null)
        {
            if (getY()>33 && getY()<46) 
            {
                MyWorld world = (MyWorld)getWorld();
                world.addScore(2);
                world.removeObject(this);
            } 
            else if (getY() < 59)
            {
                MyWorld world = (MyWorld)getWorld();
                world.addScore(1);
                world.removeObject(this);
            } 
        }
    }
}
