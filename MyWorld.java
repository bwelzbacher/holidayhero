import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Brenna Welzbacher
 */
public class MyWorld extends World
{ 
    private int time; 
    private int score;
    GreenfootSound music = new GreenfootSound("song.mp3");
    
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
        time = 2340;
        score = 0;
        showScore();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        edges edges = new edges();
        addObject(edges,49,200);
        edges edges2 = new edges();
        addObject(edges2,557,220);
        hit hit = new hit();
        addObject(hit,175,46);
        hit hit2 = new hit();
        addObject(hit2,308,53);
        hit2.setLocation(308,46);
        hit hit3 = new hit();
        addObject(hit3,445,46);
        hit3.setLocation(441,41);
        hit2.setLocation(312,40);
        hit.setLocation(169,41);
        hit2.setLocation(302,40);
        hit.setLocation(185,39);
        hit3.setLocation(409,38);
        hit2.setLocation(304,40);
        hit.setLocation(190,39);
        hit2.setLocation(302,38);
        hit2.setLocation(301,38);
    }
    
    public void act() 
    {
        countTime();
        createRed();
        createGreen();
        createYellow();
    }
    
    private void countTime() 
    {
        time--;
        playSong();
        if (time == 1250)
        {
            Greenfoot.stop();
            showText("GAME OVER!", 300, 300);
            showText("Final Score: " + score, 300, 320);
        }
    }
    
    private void playSong() 
    {
        if(time == 2263) 
        {
            music.play();
        }
    }

    private void createRed() 
    {
        if (time == 0) {
           addObject(new red(), 190, 420);
        }
        if (time == 2230) {
           addObject(new red(), 190, 420);
        }
        if (time == 2170) {
           addObject(new red(), 190, 420);
        }
        if (time == 2110) {
           addObject(new red(), 190, 420);
        }
        if (time == 2050) {
           addObject(new red(), 190, 420);
        }
        if (time == 2020) {
           addObject(new red(), 190, 420);
        }
        if (time == 1990) {
           addObject(new red(), 190, 420);
        }
        if (time == 1930) {
           addObject(new red(), 190, 420);
        }
        if (time == 1810) {
           addObject(new red(), 190, 420);
        }
        if (time == 1780) {
           addObject(new red(), 190, 420);
        }
        if (time == 1750) {
           addObject(new red(), 190, 420);
        }
        if (time == 1690) {
           addObject(new red(), 190, 420);
        }
        if (time == 1630) {
           addObject(new red(), 190, 420);
        }
        if (time == 1570) {
           addObject(new red(), 190, 420);
        }
        if (time == 1510) {
           addObject(new red(), 190, 420);
        }
        if (time == 1450) {
           addObject(new red(), 190, 420);
        }
        if (time == 1390) {
           addObject(new red(), 190, 420); 
        } 
    }
    
    private void createGreen() 
    {
        if (time == 0) {
           addObject(new green(), 299, 420);
        }
        if (time == 2208) {
           addObject(new green(), 299, 420);
        }
        if (time == 2148) {
           addObject(new green(), 299, 420);
        }
        if (time == 2088) {
           addObject(new green(), 299, 420);
        }
        if (time == 2050) {
           addObject(new green(), 299, 420);
        }
        if (time == 1960) {
           addObject(new green(), 299, 420);
        }
        if (time == 1900) {
           addObject(new green(), 299, 420);
        }
        if (time == 1840) {
           addObject(new green(), 299, 420);
        }
        if (time == 1810) {
           addObject(new green(), 299, 420);
        }
        if (time == 1720) {
           addObject(new green(), 299, 420);
        }
        if (time == 1600) {
           addObject(new green(), 299, 420);
        }
        if (time == 1390) {
           addObject(new green(), 299, 420); 
        } 
    }
    
    private void createYellow() 
    {
        if (time == 0) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 2200) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 2140) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 2080) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 2050) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 1870) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 1810) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 1660) {
           addObject(new yellow(), 409, 420);
        }
        if (time == 1390) {
           addObject(new yellow(), 409, 420); 
        } 
    }
    
    public void addScore(int points)
    {
        score = score + points;
        showScore();
    }
    
    private void showScore() 
    {
        showText("Score: " + score, 60, 25);
    }
    

    

    
    
    

    
   
    
    
}
